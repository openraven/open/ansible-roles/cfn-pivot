---
ansible_ssh_common_args: "-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
ansible_python_interpreter: /opt/bin/python

api_base_url: https://api.openraven.com/api

awscli_exe: '{{ (ansible_playbook_python|dirname) + "/aws" }}'
cfn_signal_exe: /opt/aws-cfn-bootstrap/bin/cfn-signal
# https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-cloudformation-stackset.html#cfn-cloudformation-stackset-templatebody
maximum_stack_template_length: 51200

sentry_cli_exe: /opt/bin/sentry-cli
sentry_dsn: https://35527e058ac344fabe051b31c6e17098@o322024.ingest.sentry.io/5269206

# we cannot use the ansible openssl tasks:
# ImportError: /opt/ansible/site-packages/cryptography/hazmat/bindings/
#   _openssl.pypy36-pp73-x86_64-linux-gnu.so: symbol EC_POINT_set_affine_coordinates_GF2m version OPENSSL_1_1_0
#   not defined in file libcrypto.so.1.1 with link time reference
cfssl_url: https://github.com/cloudflare/cfssl/releases/download/v1.5.0/cfssl_1.5.0_linux_amd64
cfssl_checksum: sha256:d5cea17ec190dd19ea300366c6fd7d2b41c8d9a599b30be79c290d1477f82c68

# <editor-fold desc="KUBEADM DEFAULTS">
# this lines up with the major.minor of docs.calicoproject releases:
# https://github.com/projectcalico/calico/releases
# since it is plugged into https://docs.calicoproject.org/HERE/manifests/calico.yaml
calico_version: v3.17
cni_plugin: "calico"
cri_socket: /run/containerd/containerd.sock

# https://github.com/kubernetes-sigs/etcdadm/releases
etcdadm_version: v0.1.3
# https://github.com/kubernetes-sigs/etcdadm/releases/download/v0.1.3/etcdadm-linux-amd64-sha256
etcdadm_sha256: 1cc781d15cb5994eb9918c9f6947a00481c83494e7fe86eb9aac8ffe70bdfa96
kubernetes_version: v1.19.8

kubeadm_config_path: /root/kubeadm-config.yaml
kubeadm_verbosity: 100

# this is the *listen* address
kubernetes_api_endpoint_port: 6443
# kubernetes_api_server_address
# this is the ELB's port, appended to {{ kubernetes_api_server_address }}
kubernetes_api_server_port: 6443
# kubeadm_cert_key
# cluster_name
# kubeadm_token
kubeadm_token_ttl: "0"


# language=yaml
kubeadm_api_server_extra_args: |
  audit-log-path: "-"
  audit-policy-file: /etc/kubernetes/audit.yaml
  cloud-provider: aws
  cloud-config: /etc/kubernetes/cloud_config

  oidc-client-id: {{ KUBE_SERVICE_CLIENT_ID }}
  oidc-issuer-url: {{ KUBE_SERVICE_CLIENT_ISSUER_URL }}
  oidc-username-claim: "preferred_username"
  oidc-username-prefix: "okta:"
  oidc-groups-claim: "groups"
  oidc-groups-prefix: "okta:"

# language=yaml
kubeadm_api_server_extra_volumes: |
  - name: audit-policy
    hostPath:  /etc/kubernetes/audit.yaml
    mountPath: /etc/kubernetes/audit.yaml
    readOnly: true
    pathType: File
  - name: cloud-config
    hostPath:  /etc/kubernetes/cloud_config
    mountPath: /etc/kubernetes/cloud_config
    readOnly: true
    pathType: File

# language=yaml
kubeadm_controller_manager_extra_args: |
  cloud-provider: aws
  cloud-config: /etc/kubernetes/cloud_config
  flex-volume-plugin-dir: /var/lib/kubelet/plugins/volume/exec

# language=yaml
kubeadm_controller_manager_extra_volumes: |
  - name: cloud-config
    hostPath:  /etc/kubernetes/cloud_config
    mountPath: /etc/kubernetes/cloud_config
    readOnly: true
    pathType: File

# if you put it here, it'll unfortunately label the control-plane, also
#   node-labels: "node.kubernetes.io/role=worker"
# language=yaml
kubeadm_kubelet_extra_args: |
  cloud-provider: aws
  cloud-config: /etc/kubernetes/cloud_config


# </editor-fold>

# <editor-fold desc="HELMFILE0 DEFAULTS">
# it's named this way because that's how cloud-init refers to it
# even though we're on flatcar
coreos_reboot_strategy: "off"

# these two configure "cluster-upgrade", who watches S3
helm_s3_bucket: openraven-deploy
helm_s3_key: charts/helmfile.yaml

# and this is for "helmfile0" which simply uses curl to fetch it once
helmfile_url: https://{{ helm_s3_bucket }}.s3.amazonaws.com/{{ helm_s3_key }}

# use our image so we can run ansible inside it
# (doing so makes the ZK defaults a bazillion times easier)
# https://gitlab.com/openraven/open/build-image/-/pipelines
helmfile_image: registry.gitlab.com/openraven/open/build-image:375386676

# the namespace(s) in which we should register the TLS for the Ingress(es)
openraven_tls_ns_list:
- ui
# for the time being, we will create this same Secret name in every {{openraven_tls_ns_list}}
openraven_tls_name: www-tls

# the namespace(s) in which we should populate the jdbc-env Secret
openraven_jdbc_ns_list:
- aws
- dmap
- ui
- cluster-upgrade
- splunk

# this is just for visibility, but please don't change it without updating helmfiles
openraven_jdbc_secret_name: jdbc-env

okta_corp_client_id: "0oa5zzwcdvNHUKhW9357"
# Uses a custom authorization server in corp okta to provide the groups bits as needed
okta_corp_issuer_url: "https://openraven.okta.com/oauth2/aus608qqsq7NncHak357"

# the default issuer URL if api.openraven.com doesn't offer one
oidc_issuer_url: https://login.openraven.net/oauth2/aus1i055qhbS5kjvB0h8

# these are declared via set_fact: but they appear here to advertise their presence
# openraven_okta_group_id:

SEGMENT_WRITE_KEY: mLZedQqPeRGL19unziVuoVQXDXjMl94F
# the boot0.sh tests this for "yes", it's not consumed by ansible
aws_ssm_enabled: 'yes'

# </editor-fold>
