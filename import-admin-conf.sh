#! /usr/bin/env bash
set -euo pipefail
if [[ $# -eq 0 ]]; then
    echo "Usage: $0 admin.conf cluster-id user-id" >&2
    exit 1
fi

# you can run this as N=echo to _see_ the kubectl it will issue
# be forewarned that N=echo does **not** prevent it from writing out files
N="${N:-}"

admin_conf="$1"
shift
if [[ ! -e "$admin_conf" ]]; then
    echo "Your admin conf \"$admin_conf\" is 404" >&2
    exit 1
fi

cluster_name="$1"
shift

user_name="$1"
shift

context_name="${user_name}@${cluster_name}"

ok=1
if [[ -e "${cluster_name}.crt" ]]; then
    echo "I will not overwrite \"${cluster_name}.crt\"" >&2
    ok=0
fi
for i in crt key; do
    if [[ -e "${user_name}.${i}" ]]; then
        echo "I will not overwrite \"${user_name}.${i}\"" >&2
        ok=0
    fi
    unset i
done
if [[ ${ok} -ne 1 ]]; then
    exit 1
fi
unset ok


dump_raw_config() {
    local jp="$1"
    kubectl --kubeconfig "$admin_conf" \
        config view -o jsonpath="$jp" --raw=true
}

dump_raw_config_b64() {
    local jp="$1"
    dump_raw_config "$jp" | base64 --decode
}

server_url=$(dump_raw_config "{.clusters[0].cluster['server']}")
dump_raw_config_b64 "{.clusters[0].cluster['certificate-authority-data']}" > ${cluster_name}.crt
dump_raw_config_b64 "{.users[0].user['client-certificate-data']}" > ${user_name}.crt
dump_raw_config_b64 "{.users[0].user['client-key-data']}"         > ${user_name}.key

${N} kubectl config set-cluster \
    --server="$server_url" \
    --embed-certs=true \
    --certificate-authority=./"${cluster_name}.crt" \
    "$cluster_name"
${N} kubectl config set-credentials \
    --client-certificate=./"${user_name}.crt" \
    --client-key=./"${user_name}.key" \
    --embed-certs=true \
    "$user_name"
${N} kubectl config set-context --cluster="$cluster_name" --user="$user_name" --namespace=ui "$context_name"
echo 'Now you can use that context as your default via:' >&2
echo "kubectl config use-context \"$context_name\"" >&2
echo "or use it per-invocation as: kubectl --context=\"$context_name\" ..." >&2
exit 0
