#! /usr/bin/env python
# coding=utf-8
import json
import re
import subprocess
import sys
from io import StringIO

"""
TASK [cfn-pivot : cfn-pivot | deconstruct activation_result into nice facts] ***
task path: /root/roles/cfn-pivot/tasks/main_tasks.yml:122
ok: [localhost] => {
    "ansible_facts": {
        "ADMIN_CLIENT_ID": "0oa1iiui7wuM79jDn0h8",
...
}
"""

log_fn = sys.argv[1]
proc = subprocess.Popen(
    ['sed', '-ne', '/^TASK.*deconstruct activation_result/,/^[}]/p', log_fn],
    stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdout, stderr = proc.communicate()
if stderr:
    print('BOGUS:\nstderr = <<{}>>\nstdout = <<{}>>\nRC={}\n'
          .format(stderr, stdout, proc.returncode), file=sys.stderr)
    sys.exit(1)
sio = StringIO(stdout.decode('utf-8'))
buf = ['{']
for line in sio.readlines():
    if re.search(r'^\S+', line):
        continue
    buf.append(line)
buf.append('}')
payload = json.loads('\n'.join(buf))
ansible_facts = payload['ansible_facts']
fact_map = {
    "feClientId": "FRONTEND_CLIENT_ID",
    "beClientId": "SERVICE_CLIENT_ID",
    "beClientSecret": "SERVICE_CLIENT_SECRET",
    "adminClientSecret": "ADMIN_CLIENT_SECRET",
    "adminClientId": "ADMIN_CLIENT_ID",
    "clusterName": "CLUSTER_NAME",
    "cookieSecret": "COOKIE_SECRET",
    "tlsPrivateKeyB64": "TLS_PRIVATE_KEY_B64",
    "tlsCertificateB64": "TLS_CERTIFICATE_B64",
}
extra_json = {
    "route53": {
        "domainName": ansible_facts['openraven_ingress_hostname']
    },
}
extra_json.update({k: ansible_facts[v] for k, v in fact_map.items()})
extra_vars = {
    "activation_result": {
        "json": extra_json,
    }
}

# this filename is referenced in __pivot__.sh
# and we are taking advantage of the fact that JSON is a subset of YAML
# to avoid dealing with whether the current virtualenv has pyyaml in it
output_fn = 'activation_result.yml'
with open(output_fn, 'w') as fh:
    json.dump(extra_vars, fh, indent=2)
print('Wrote the activation response to "{}"'.format(output_fn))
