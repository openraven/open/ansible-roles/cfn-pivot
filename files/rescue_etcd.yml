- hosts: localhost
  connection: local
  gather_facts: no
  become: no
  vars_prompt:
  - name: ansible_python_interpreter
    prompt: full path to a python interpreter that has "boto3" installed
    private: no
  - name: aws_region
    prompt: the region in which the stack lives
    private: no
  - name: cf_stack_name
    prompt: the name of the CF stack
    private: no

  tasks:
  - name: inspect the stack's CF for the resources
    cloudformation_info:
      stack_name: '{{ cf_stack_name }}'
      region: '{{ aws_region }}'
      stack_resources: yes
    register: cf_info

  - name: extract relevant details from the CF stack
    set_fact:
      bastion_ip: '{{ my_cf.stack_outputs.BastionIp }}'
      control_lc: '{{ my_cf.stack_resources.K8sControlLaunchConfig }}'
      control_subnet: '{{ my_cf.stack_resources.PrivateSubnet1a }}'
      # control_sg: '{{ my_cf.stack_resources.K8sControlSg }}'
    vars:
      my_cf: '{{ cf_info.cloudformation | dict2items | map(attribute="value") | first }}'

  - name: describe the contol LC to get its cloud-config
    ec2_lc_info:
      name: '{{ control_lc }}'
      region: '{{ aws_region }}'
    register: control_lc_info

  - name: patch out its startup Unit
    # that way, it will still write the certs to disk,
    # but won't attempt to _join_ anything
    set_fact:
      control_lc: '{{ control_lc_info.launch_configurations[0] }}'
      lc_cloud_config: >-
        {%- set _ = control_cloud_config.coreos.update({"units": []}) -%}
        {{ control_cloud_config }}
    vars:
      control_cloud_config: '{{
          control_lc_info.launch_configurations[0].user_data
          | b64decode
          | from_yaml
          }}'

  - name: make a new temporary LC
    ec2_lc:
      name: etcd-recovery
      image_id: '{{ control_lc.image_id }}'
      instance_type: '{{ control_lc.instance_type }}'
      key_name: '{{ control_lc.key_name }}'
      region: '{{ aws_region }}'
      security_groups: '{{ control_lc.security_groups }}'
      user_data: '{{ lc_cloud_config | to_yaml }}'

  - name: make a temprorary ASG to launch 3 etcd instances
    ec2_asg:
      name: etcd-recovery
      availability_zones:
      - '{{ aws_region }}a'
      region: '{{ aws_region }}'
      launch_config_name: etcd-recovery
      desired_capacity: 3
      max_size: 3
      min_size: 3
      vpc_zone_identifier:
      - '{{ control_subnet }}'
    register: the_asg

  - name: describe the ASG instances
    # in order to obtain their PrivateIpAddress et al
    ec2_instance_info:
      instance_ids: '{{ the_asg.instances }}'
      region: '{{ aws_region }}'
    register: ec2_instances

  - name: put the new instances in a group
    add_host:
      name: '{{ item.private_dns_name }}'
      ansible_ssh_user: core
      # this playbook assumes(!!!) you have ssh-agent or ssh config configured already
      # ansible_ssh_private_key_file:
      ansible_host: '{{ item.private_ip_address }}'
      ansible_python_interpreter: /opt/bin/python
      ansible_ssh_common_args: >-
        -o UserKnownHostsFile=/dev/null
        -o StrictHostKeyChecking=no
        -J core@{{ bastion_ip }}
      groups:
      - the_etcds
    with_items: '{{ ec2_instances.instances }}'

- hosts: the_etcds[0]
  become: yes
  gather_facts: no
  vars_prompt:
  - name: etcd_snapshot_filename
    prompt: the etcd snapshot filename, full or relative to THE PLAYBOOK FILENAME
    private: no
    # FIXME: it may be possible to update this to snap an existing control plane
    # and use that filename

  tasks:
  - name: upload the snap file to the first new etcd
    copy:
      src: '{{ etcd_snapshot_filename }}'
      dest: /tmp

  - name: ensure etcdadm
    get_url:
      dest: /opt/bin/etcdadm
      url: https://openraven-deploy.s3.amazonaws.com/bin/etcdadm
      mode: '0755'

  - name: initialize the new cluster
    command: >-
      /opt/bin/etcdadm init
      --certs-dir /etc/kubernetes/pki/etcd
      --name {{ inventory_hostname }}
      --snapshot /tmp/etcd.snap
    args:
      creates: /etc/etcd/etcd.env

  - name: ensure a-ok
    command: /opt/bin/etcdctl.sh member list

- hosts: the_etcds
  # it's safe to visit them all, due to the "creates:" guard
  become: yes
  gather_facts: no
  # they have to roll into place one at a time
  serial: 1
  tasks:
  - name: compose the initial join-url
    set_fact:
      etcdadm_join_url: 'https://{{ hostvars[groups["the_etcds"]|first].ansible_host }}:2379'

  - name: ensure etcdadm
    get_url:
      dest: /opt/bin/etcdadm
      url: https://openraven-deploy.s3.amazonaws.com/bin/etcdadm
      mode: '0755'

  - name: join to the new etcd cluster
    command: >-
      /opt/bin/etcdadm join
      --certs-dir /etc/kubernetes/pki/etcd
      --name {{ inventory_hostname }}
      {{ etcdadm_join_url }}
    args:
      creates: /etc/etcd/etcd.env

  - name: start etcd.service
    systemd:
      name: etcd.service
      state: started
      enabled: yes
      daemon_reload: yes

  - name: ensure a-ok
    command: /opt/bin/etcdctl.sh member list

  - name: message
    debug:
      msg: |
        now ssh into any surviving control plane node:

        systemctl stop kubelet.service

        systemctl stop etcd.service

        /opt/bin/etcdadm reset --skip-remove-member

        # just for good measure, although these should already be gone
        rm -rf /etc/etcd /var/lib/etcd

        /opt/bin/etcdadm join --certs-dir /etc/kubernetes/pki/etcd --name $(hostname -f) {{ etcdadm_join_url }}

        # ensure it came up ok, and it can see the other peers
        /opt/bin/etcdctl.sh member list

        systemctl start kubelet.service

        then keep an eye out for the Node to re-appear in kubectl get nodes

        after that, you can walk over the recovery etcd members doing:

        /opt/bin/etcdctl.sh member remove $(
          /opt/bin/etcdctl.sh member list | awk -F, "/$(hostname -f),/{print \$1}"
        )
        systemctl stop etcd.service
