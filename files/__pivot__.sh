#! /usr/bin/env bash
set -euo pipefail

ATTEMPT_NUMBER="${ATTEMPT_NUMBER:-0}"

if [[ -e pivot.log ]]; then
  mv     pivot.log ".pivot.$(date +%s).log"
fi

cat >pivot.yml<<'YML'
- hosts: localhost
  connection: local
  vars:
    ansible_python_interpreter: /opt/ansible/bin/python
  roles:
  - cfn-pivot
YML

# until ENG-2249 lands, load these vars if present
activation_result_fn=activation_result.yml
if [[ -e "$activation_result_fn"        ]]; then
  set -- -e "@./$activation_result_fn" "$@"
fi

echo '== ENV' >&2
env
echo '==/ENV' >&2

if [[ ! -x /opt/bin/sentry-cli ]]; then
  if curl -fsSLo /opt/bin/sentry-cli https://downloads.sentry-cdn.com/sentry-cli/1.54.0/sentry-cli-Linux-x86_64; then
    chmod 0755   /opt/bin/sentry-cli || true
  else
    echo 'Unable to fetch sentry-cli' >&2
  fi
fi

if [[ -z "${SENTRY_RELEASE:-}" ]]; then
  if [[ -e cfn-pivot.tar ]]; then
    # make the release monotonically increasing
    SENTRY_RELEASE=$(stat --format='%Y' cfn-pivot.tar || true)
    export SENTRY_RELEASE
  fi
fi

if ! /opt/ansible/bin/ansible-playbook \
      -i /etc/ansible/hosts \
      -c local \
      -vvv \
      "$@" \
      pivot.yml 2>&1|tee pivot.log
then
  if [[ $ATTEMPT_NUMBER -gt 3 ]]; then
    echo 'Exceeded retry attempts, that is fatal' >&2
    exit 1
  fi
  echo 'Going around for a retry in 10s ...' >&2
  sleep 10
  if [[ ! -e "$activation_result_fn" ]]; then
    ./roles/cfn-pivot/files/activation_result.py pivot.log
  fi
  ./roles/cfn-pivot/files/reset_cfn.sh
  ATTEMPT_NUMBER=$(( ATTEMPT_NUMBER + 1 ))
  export ATTEMPT_NUMBER
  # we want the log file rotation, and activation_result detection
  # so call ourselves "recursively"
  exec "$0" "$@"
fi
