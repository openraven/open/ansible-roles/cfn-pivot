#! /usr/bin/env bash
set -euo pipefail
. /root/boot0.fn.sh
trap rescue EXIT

ksys() {
  kubectl -n kube-system "$@"
}

pod_phase() {
  ksys get -o jsonpath='{.status.phase}' "$1"
}

pod_exit_code() {
  # thankfully, asking for a non-existent field in jsonpath
  # it not an error, just the empty string
  ksys get -o jsonpath='{.status.containerStatuses[0].state.terminated.exitCode}' "$1"
}

now=$(date +%s)
poll_minutes=20
poll_timeout=$(( now + ( poll_minutes * 60 ) ))
while [[ $(date +%s) -lt $poll_timeout ]]; do
  echo "== BEGIN ALL NODES" >&2
  kubectl get -o wide nodes
  echo "== END ALL NODES" >&2

  echo "== BEGIN ALL PODS" >&2
  kubectl get -o wide --all-namespaces=true pods
  echo "== END ALL PODS" >&2

  pod_name=$(ksys get -o name pods -l app.kubernetes.io/name=helmfile0)
  if [[ -z "$pod_name" ]]; then
    sleep 10
    continue
  fi
  if ! ksys get -o json "$pod_name" | jq '.status'; then
    continue
  fi
  p_phase="$(pod_phase "$pod_name")"
  if [[ Succeeded == "$p_phase" ]]; then
    break
  fi
  echo "== BEGIN LOGS" >&2
  # turns out, asking for logs of a Pending pod is rc=1
  ksys logs -f "$pod_name" || true
  echo "== END LOGS (rc=$?)" >&2
  sleep 60
done

set -e
if [[ -z "${pod_name:-}" ]]; then
  echo 'Bombing the deploy due to no discovered pod name' >&2
  false
fi

ksys logs "$pod_name" >helm0.log 2>&1
# watch out, this assumes the script is running set-x
if ! grep -q 'send_outcome_post success' helm0.log; then
  cat helm0.log
  false
fi

echo 'All done, shutting down'
shutdown -h now
