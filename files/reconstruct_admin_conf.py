#! /usr/bin/env python
# coding=utf-8
import json
import re
import subprocess
import sys
try:
    import yaml
except ImportError:
    print('You will want a python with "pyyaml" installed\n\n',
          file=sys.stderr)
    raise

if len(sys.argv) > 1 and '-c' == sys.argv[1]:
    print('Loading "aws cloudformation" JSON from stdin ...',
          file=sys.stderr)
    cf = json.load(sys.stdin)
    cf_outputs = {
        o['OutputKey']: o['OutputValue']
        for o in cf['Stacks'][0]['Outputs']
    }

    def get_or_exit(o_key):
        result = cf_outputs[o_key]
        if not result.strip():
            print('Expected \"{}\" to be non-empty',
                  file=sys.stderr)
            sys.exit(1)
        return result
    b64_parts = get_or_exit('AdminConfB64')
    endpoint = get_or_exit('KubernetesApiUrl')
else:
    b64_parts = input('AdminConfB64? ')
    endpoint = input('KubernetesApiUrl? ')
endpoint = re.sub(r'/$', '', endpoint.strip())

a_b = b64_parts.split(' ')
admin_cert_b64 = a_b[0]
admin_key_b64 = a_b[1]

# this is just some damn silliness, as it says Unauthorized even for kube-public
#     c_info_cmd = [
#         'kubectl', '--insecure-skip-tls-verify=true',
#         '--server={}'.format(endpoint),
#         '--namespace=kube-public',
#         '--request-timeout=15s',
#         'get', '--output=json',
#         'configmap/cluster-info'
#     ]
c_info_cmd = [
    'curl',
    '--insecure',
    '--header',
    'accept: application/json',
    '--fail',
    '--silent',
    '--show-error',
    '{}/api/v1/namespaces/kube-public/configmaps/cluster-info'.format(endpoint)
]
# print(" ".join(c_info_cmd), file=sys.stderr)
proc = subprocess.Popen(
    c_info_cmd,
    stdout=subprocess.PIPE, stderr=subprocess.PIPE)

stdout, stderr = proc.communicate()
if stderr:
    print('BOGUS:\nstderr:<<{}>>\nstdout=<<{}>>\nrc={}\n'
          .format(stderr, stdout, proc.returncode), file=sys.stderr)
    sys.exit(1)

c_info_dict = json.loads(stdout)

kubeconfig_yml = c_info_dict['data']['kubeconfig']
kubeconfig = yaml.safe_load(kubeconfig_yml)

cluster0 = kubeconfig['clusters'][0]
cluster_name = cluster0['name']
if not cluster_name:
    cluster_name = re.sub(r'https://([^.]+)\..+', r'\1', endpoint)
    cluster0['name'] = cluster_name
user_name = '{}-admin'.format(cluster_name)

u = kubeconfig.get('users')
if u is None:
    u = []
kubeconfig['users'] = u
u.append({
    "name": user_name,
    "user": {
        "client-certificate-data": admin_cert_b64,
        "client-key-data": admin_key_b64,
    }
})
c = kubeconfig.get('contexts')
if c is None:
    c = []
kubeconfig['contexts'] = c
c.append({
    "name": cluster_name,
    "context": {
        "cluster": cluster_name,
        "user": user_name,
        "namespace": "ui",
    }
})
kubeconfig['current-context'] = cluster_name

cluster_config_fn = '{}.yml'.format(cluster_name)
with open(cluster_config_fn, 'w') as fh:
    yaml.dump(kubeconfig, fh)
print('Wrote cluster config to "{}"'.format(cluster_config_fn))
