#! /usr/bin/env bash
set -euo pipefail
exec /opt/ansible/bin/ansible-playbook -vv -c local -i localhost, "$@" "$(dirname "$0")/$(basename "$0" .sh).yml"
