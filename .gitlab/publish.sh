#! /usr/bin/env bash
set -euo pipefail

TRACE="${TRACE:-}"
[[ -n "$TRACE" ]] && set -x


# apologies for the name, but it's likely a consistency win
HELM_S3_BUCKET="${HELM_S3_BUCKET:-}"
if [[ -z "${HELM_S3_BUCKET}" ]]; then
    echo 'Not without HELM_S3_BUCKET' >&2
    exit 1
fi

CI_COMMIT_SHA="${CI_COMMIT_SHA:-}"
if [[ -z "${CI_COMMIT_SHA}" ]]; then
    CI_COMMIT_SHA="$(git rev-parse HEAD)"
fi

tar_fn=cfn-pivot.tar
git archive --format tar --output cfn-pivot.tar "$CI_COMMIT_SHA"

aws s3 cp "$tar_fn" "s3://${HELM_S3_BUCKET}/$tar_fn"

.gitlab/generate_manifest.py $tar_fn

aws s3 cp bootstrap_cfn_pivot_manifest.json "s3://${HELM_S3_BUCKET}/bootstrap_cfn_pivot_manifest.json"