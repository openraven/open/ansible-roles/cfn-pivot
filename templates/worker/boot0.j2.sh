#! /usr/bin/env bash
. /root/boot0.env

if [[ "${AWS_SSM_ENABLED:-}" == yes ]]; then
  systemctl enable amazon-ssm-agent
  systemctl start  amazon-ssm-agent
fi

rescue() {
  # shellcheck disable=SC2181
  if [[ $? -eq 0 ]]; then
    return
  fi
  curl -fsS -H 'content-type: application/json' \
       -u "${SEGMENT_WRITE_KEY}:" \
       --data-binary "$(jq -n --arg deployChannel "$DEPLOY_CHANNEL" \
         --arg awsRegion "$AWS_DEFAULT_REGION" \
         --arg cfnStackName "$AWS_CFN_STACK_NAME" \
         --arg userId "$GROUP_ID" \
         '{
           event: "NodeJoinFailure",
           userId: $userId,
           context: {awsRegion: $awsRegion,
                     cfnStackName: $cfnStackName,
                     deployChannel: $deployChannel}
         }')" \
       https://api.segment.io/v1/track
  # no need for shutdown like with the control plane
  # since the ELB health check will kill the instance anyway
}
trap rescue EXIT

# this comment hides the jinja mustaches from shellcheck
# {{ "\n" + boot0_upgrade_sh }}

set -euo pipefail
# wait for containerd readiness
for _ in $(seq 1 30); do
  if crictl ps; then
    break
  fi
  sleep 1
done
# unknown why this isn't propagating on its own
kubelet_env_fn=/etc/default/kubelet
if ! grep -q cloud-provider $kubelet_env_fn 2>/dev/null; then
  if [[ ! -s $kubelet_env_fn ]]; then
    # doing this blows up the containerd detection
    # echo 'KUBELET_EXTRA_ARGS="--node-labels=node.kubernetes.io/role=worker"' > $kubelet_env_fn
    echo 'KUBELET_EXTRA_ARGS=""' > $kubelet_env_fn
  fi
  sed -i~ -e 's@"$@ --cloud-provider=aws --cloud-config=/etc/kubernetes/cloud_config"@' $kubelet_env_fn
fi
systemctl enable kubelet.service
mkdir -p /etc/kubernetes/manifests
kubeadm --v=1000 join "$API_ENDPOINT" \
     --token "$JOIN_TOKEN" --discovery-token-ca-cert-hash "$CA_CERT_HASH" \
     --certificate-key "$CERT_KEY"
crictl ps -a || true
export KUBECONFIG=/etc/kubernetes/kubelet.conf
kubectl label node "$(hostname -f)" node.kubernetes.io/role=worker || true
