#! /usr/bin/env bash
. /root/boot0.env

if [[ "${AWS_SSM_ENABLED:-}" == yes ]]; then
  systemctl enable amazon-ssm-agent
  systemctl start  amazon-ssm-agent
fi

rescue() {
  # shellcheck disable=SC2181
  if [[ $? -eq 0 ]]; then
    return
  fi
  set +e
  curl -fsS -H 'content-type: application/json' \
       -u "${SEGMENT_WRITE_KEY}:" \
       --data-binary "$(jq -n --arg deployChannel "$DEPLOY_CHANNEL" \
         --arg eventName "${SEGMENT_EVENT:-NodeJoinFailure}" \
         --arg awsRegion "$AWS_DEFAULT_REGION" \
         --arg cfnStackName "$AWS_CFN_STACK_NAME" \
         --arg userId "$GROUP_ID" \
         '{
           event: $eventName,
           userId: $userId,
           context: {awsRegion: $awsRegion,
                     cfnStackName: $cfnStackName,
                     deployChannel: $deployChannel}
         }')" \
       https://api.segment.io/v1/track
    sleep 2  # give the curl time to clean up
    shutdown -h now
    exit 1
}
trap rescue EXIT

# this comment hides the jinja mustaches from shellcheck
# {{ "\n" + boot0_upgrade_sh }}

set -euo pipefail
cd /root

if [[ ! -x /opt/bin/etcdadm ]]; then
  for _ in 1 2 3; do
    if ! curl -fsSLo /opt/bin/etcdadm "https://$DEPLOY_CHANNEL.s3.amazonaws.com/bin/etcdadm"; then
      sleep 2
      continue
    fi
    chmod 0755  /opt/bin/etcdadm
    break
  done
fi

if [[ ! -e /etc/etcd/etcd.env ]]; then
  control_inst_filter() {
     jq -n --arg stack_name "$AWS_CFN_STACK_NAME" '[
     {Name: "tag-value", Values: [$stack_name]},
     {Name: "tag-value", Values: ["master"]},
     {Name: "instance-state-name", Values: ["running"]}
     ]'
  }
  etcd_urls() {
    aws ec2 describe-instances \
       --filter "$(control_inst_filter)" \
    | jq --arg my_name "$HOSTNAME" -r '[
       .Reservations[].Instances
       | sort_by(.LaunchTime)[] as $i
       | select($i.PrivateDnsName != $my_name)
       | "https://\($i.PrivateDnsName):2379"
       ] | join(" ")'
  }

  ok=0
  for _ in 1 2 3; do
     for ETCDADM_ENDPOINT in $(etcd_urls)
     do
       if etcdadm join "$ETCDADM_ENDPOINT" \
          --certs-dir /etc/kubernetes/pki/etcd --log-level debug; then
         ok=1
         break
       fi
       sleep 5
    done
    if [[ $ok -eq 1 ]]; then
      break
    fi
  done

  if [[ $ok -eq 0 ]]; then
    echo "Unable to bootstrap etcd in 3 tries" >&2
    # commit suicide to more clearly indicate things are unwell
    SEGMENT_EVENT=EtcdJoinFailure
    false
  fi
fi
SEGMENT_EVENT=ControlJoinFailure

# unknown why this isn't propagating on its own
kubelet_env_fn=/etc/default/kubelet
if ! grep -q cloud-provider $kubelet_env_fn 2>/dev/null; then
  if [[ ! -s $kubelet_env_fn ]]; then
    echo 'KUBELET_EXTRA_ARGS=""' > $kubelet_env_fn
  fi
  sed -i~ -e 's@"$@ --cloud-provider=aws --cloud-config=/etc/kubernetes/cloud_config"@' $kubelet_env_fn
fi
systemctl enable kubelet.service
kubeadm --v=1000 join "$API_ENDPOINT" --control-plane \
    --token "$JOIN_TOKEN" --discovery-token-ca-cert-hash "$CA_CERT_HASH" \
    --certificate-key "$CERT_KEY" 2>&1|tee join.log
crictl ps -a || true
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl label node --overwrite=true "$(hostname -f)" \
   node-role.kubernetes.io/master= role.kubernetes.io/role=master
